//класс пользователя
/*
class Person{
	String first, last
}
//функции без типизированных параметров
def printInfo(first, second){
	println "first name: $first, second name: $second"
}
def printFirstName(user){
	println "user: $user.first"
}
//создаем обьект класса Person с параметрами
def tempPerson = new Person(first: 'Adam', last: 'Smith')
// вызов функции разными способами
printInfo tempPerson.first, tempPerson.last
printFirstName(tempPerson)
*/

/*
def hu(){
	println "hu"
}

hu()
*/
def iFact = { (it > 1) ? (2..it).inject(1) { i, j -> i*j } : 1 }
println "${50}: ${iFact(50)}"