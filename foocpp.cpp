#include <iostream>
using namespace std;

void hu(int x){
	cout<<"hu "<<x<<endl;
}

int factorial(int n) {
	if (n < 0 ) {
		return 0;
	}
	return !n ? 1 : n * factorial(n - 1);
}

int main(){
	cout<<"main"<<endl;
	hu(5);
	cout<<factorial(50)<<endl;
	return 0;
}