% hello world program
-module(hw).
-export([start/0]).

start() ->
    io:fwrite("Hello, world!\n").