#include <stdio.h>

int factorial(int n) {
	if (n < 0 ) {
		return 0;
	}
	return !n ? 1 : n * factorial(n - 1);
}

void main(){
	int x = factorial(120);
	printf("%d\n", x);    	
}