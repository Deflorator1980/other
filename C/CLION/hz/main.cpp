#include <iostream>

using namespace std;

template <int SIZE>
class Test
{
    char buffer[SIZE];
public:
    void fillBuffer(char *buf)
    {

    }

    constexpr static int getBufferSize()
    {
        return SIZE*2;
    }
};

typedef Test<8> MyTest;

int main()
{
    char buf[MyTest::getBufferSize()];
    MyTest t;
    t.fillBuffer(buf);

    return 0;
}