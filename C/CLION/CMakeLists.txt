cmake_minimum_required(VERSION 3.3)
project(One)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp memory_manag.cpp memory_manag.h)
add_executable(One ${SOURCE_FILES})