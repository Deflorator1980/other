#include <iostream>
using namespace std;

int factorial(int n) {
	if (n < 0 ) {
		return 0;
	}
	return !n ? 1 : n * factorial(n - 1);
}

int main(){
	// cout<<factorial(50)<<endl;
	double x = 0.1 + 0.2;
	cout<<x<<endl;
	return 0;
}