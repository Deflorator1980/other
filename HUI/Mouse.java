tatic class Resource implements AutoCloseable {
public void work() throws Exception {
System.out.print("3");
throw new Exception();
}
public void close() throws Exception {
System.out.print("4");
throw new Exception();
}
}
public static void main(String[] args) {
try (Resource res = new Resource()) {
System.out.print("1");
res.work();
} catch (Exception e) {
System.out.println("2");
}
}