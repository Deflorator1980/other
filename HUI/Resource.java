public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Person e = new Person("John");
        FileOutputStream fs =  new FileOutputStream("e.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fs);
        oos.writeObject(e);
        oos.close();
        fs.close();
        FileInputStream fis = new FileInputStream("e.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);
        e = (Person) ois.readObject();
        System.out.println(e.name);
    }
}