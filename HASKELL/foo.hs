doubleMe x = x + x
factorial :: Integer -> Integer
factorial n = product [1..n]

circumference :: Float -> Float
circumference r = 2 * pi * r

circumference2 :: Double -> Double
circumference2 r = 2 * pi * r

bmiTell :: Double -> Double -> String
bmiTell weight height
	| weight / height ^ 2 <= 18.5 = "<= 18.5"  -- + weight / height
	| weight / height ^ 2 <= 25.0 = "<= 25.0"
	| weight / height ^ 2 <= 30.0 = "<= 30.0"
	| otherwise = "otherwise"

max' :: (Ord a) => a -> a -> a
max' a b
	|a <= b = b
	|otherwise = a	

myCompare :: (Ord a) => a -> a -> Ordering
a `myCompare` b
	| a == b = EQ
	| a <=b = LT
	| otherwise = GT	

bmiTell2 :: Double -> Double -> String
bmiTell2 weight height
	| bmi<= 18.5 = "<= 18.5"  
	| bmi <= 25.0 = "<= 25.0"
	| bmi <= 30.0 = "<= 30.0"
	| otherwise = "otherwise"
    where bmi = weight/height ^ 2

bmiTell3 :: Double -> Double -> String
bmiTell3 weight height
	| bmi<= skinny = "<= 18.5"  
	| bmi <= normal = "<= 25.0"
	| bmi <= fat = "<= 30.0"
	| otherwise = "otherwise"
    where bmi = weight/height ^ 2  
    	  skinny = 18.5
    	  normal = 25.0
          fat = 30.0  
--greet
badGreeting :: String
badGreeting = "badGreeting"

niceGreeting :: String
niceGreeting = "niceGreeting"

greet :: String -> String
greet "hui" = niceGreeting ++ " hui"
greet "pisda" = niceGreeting ++ " pisda"
greet "blyad" = badGreeting ++ " blyad"
greet name = badGreeting ++ " " ++name
--greet
