import javax.script.*;
import java.io.*;

public class EvalScript {

    public static void main(String[] args) throws Exception {
        // create a script engine manager
        ScriptEngineManager factory = new ScriptEngineManager();
        // create a Nashorn script engine
        ScriptEngine engine = factory.getEngineByName("nashorn");
        // evaluate JavaScript statement
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            engine.eval(br);
        } catch (final ScriptException se) { se.printStackTrace(); }
    }

    public static class MyClass {
        public void printMsg(String msg) {
            System.out.println("printMsg : "+msg);
        }
    }
}
// http://habrahabr.ru/post/195870/