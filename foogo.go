package main

import "fmt"

func hu() {
	fmt.Println("hu")
}

func Factorial(n int) int {
    if n == 0 {
        return 1
    }
    return n * Factorial(n - 1)
}

func main() {
	fmt.Println("main")
	hu()
	fmt.Println(Factorial(50))
}