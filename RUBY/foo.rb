def fac x 
	x > 1 ? x * fac(x - 1) : 1
end

puts fac 50

# ------------------------------------

# def add(a_number, another_number)
#   a_number + another_number
# end

# puts add(1, 2)