(ns desktop.core
  (:require [seesaw.core :as s])
  (:gen-class))

(defn center!
  [frame]
  (.setLocationRelativeTo frame nil))

(defn -main
  [& args]
  (s/invoke-later
    (doto (s/frame :title "desktop"
                   :content "Welcome to desktop!"
                   :on-close :exit
                   :size [300 :by 300])
      center!
      s/show!)))
