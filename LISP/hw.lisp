; (write-line "hui!")
; (write-line "pisda!")

; ; (write-line "single quote used, it inhibits evaluation")
; (write '(* 2 3))
; (write-line "")
; (write "sosat")	
; ; (write-line "single quote not used, so expression evaluated")
; (write (* 2 3))

(let ((x 'a) (y "b")(z 'c))
(format t "x = ~a y = ~a z = ~a" x y z))